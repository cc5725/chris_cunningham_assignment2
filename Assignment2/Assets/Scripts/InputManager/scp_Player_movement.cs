using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_movement : MonoBehaviour
{

    //Variable that references the control scheme
    public Player_Control_Scheme controls;

    //Rigidbody ref

    private Rigidbody2D rb;

    //How fast player moves
    public float speed;

    public float thrust = 50;

    //Player attack logic
   [SerializeField] scp_Player_Attack attackLogic;


    //Dir var
    public float horizontalValue;

    //Animator script reference 
    public scp_Player_AnimationController anim;

    private void Awake()
    {

        anim = GetComponent<scp_Player_AnimationController>();

        controls = new Player_Control_Scheme();

        //References to the methods for controls
        controls.Player_PC.HorizontalMovement.performed += _ => HorizontalMovementPressed();

        controls.Player_PC.Dash.performed += _ => DashPressed();

        controls.Player_PC.Jump.performed += _ => JumpPressed();

        controls.Player_PC.Attack.performed += _ => Attack1Pressed();

        attackLogic = GetComponentInChildren<scp_Player_Attack>();
    }


    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Find rigid body and put into rb var
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        StoreHorizontalValue();
        MovePlayer();
    }

    void HorizontalMovementPressed()
    {
        FlipThePlayer();
        
    }
    void DashPressed()
    {
       
    }
   void Attack1Pressed()
    {
        anim.Attack1();

       // attackLogic.AttackLogic();
    }
    void JumpPressed()
    {
        //rb.AddForce(transform.up * thrust * Time.deltaTime, ForceMode2D.Impulse);
    }

    void FlipThePlayer()
    {
        if (controls.Player_PC.HorizontalMovement.ReadValue<float>() < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void StoreHorizontalValue()
    {
        horizontalValue = controls.Player_PC.HorizontalMovement.ReadValue<float>();
    }
    void MovePlayer()
    {
      
        Vector2 force = new Vector2(horizontalValue * speed * Time.deltaTime, 0);


        //Add force to rigidbody
        rb.AddForce(force, ForceMode2D.Impulse); 
    }
}
