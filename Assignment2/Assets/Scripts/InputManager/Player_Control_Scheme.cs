// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/InputManager/Player_Control_Scheme.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Player_Control_Scheme : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Player_Control_Scheme()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Player_Control_Scheme"",
    ""maps"": [
        {
            ""name"": ""Player_PC"",
            ""id"": ""c97b954a-bffe-4711-9c63-31c31348e9ac"",
            ""actions"": [
                {
                    ""name"": ""Horizontal Movement"",
                    ""type"": ""Button"",
                    ""id"": ""bf38ecab-db25-4568-9c0b-ef6d70bf7aaa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""a1d30be0-cb01-42cd-bf9b-d89514affbfa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""c0025688-4dc8-4e1a-b4a3-9d07a4921514"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""475beaa4-d25f-4147-a330-78a5f2a644df"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis - Keyboard"",
                    ""id"": ""c94a847a-ada1-4939-99f2-e294a74e4119"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""0fc112f9-3ca7-4344-8dae-2abe049c2fa3"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9bbec664-24cc-4bd7-8a3d-ed8df7f64b8b"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis - Gamepad"",
                    ""id"": ""1385130d-503e-4faa-8d30-83a766f1b500"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f66a423c-4d65-4b72-9205-30d088c7449d"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""e55f9d94-5c62-44ba-8e76-7bc6917011c6"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""2c659a5b-8882-4e6c-a6b7-a3a1564c12aa"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""da25ce4e-581b-4a30-8cf3-e0f4ad703541"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""133fc198-3370-440b-bdf1-3a8ec96391b1"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bb4377e9-a64e-4288-b958-adbb8731ceda"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0f9a60d5-fa6d-4e2f-99f3-2258182c29e6"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c06d85ed-20fa-49d7-9315-7107db2662de"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player_PC
        m_Player_PC = asset.FindActionMap("Player_PC", throwIfNotFound: true);
        m_Player_PC_HorizontalMovement = m_Player_PC.FindAction("Horizontal Movement", throwIfNotFound: true);
        m_Player_PC_Attack = m_Player_PC.FindAction("Attack", throwIfNotFound: true);
        m_Player_PC_Jump = m_Player_PC.FindAction("Jump", throwIfNotFound: true);
        m_Player_PC_Dash = m_Player_PC.FindAction("Dash", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player_PC
    private readonly InputActionMap m_Player_PC;
    private IPlayer_PCActions m_Player_PCActionsCallbackInterface;
    private readonly InputAction m_Player_PC_HorizontalMovement;
    private readonly InputAction m_Player_PC_Attack;
    private readonly InputAction m_Player_PC_Jump;
    private readonly InputAction m_Player_PC_Dash;
    public struct Player_PCActions
    {
        private @Player_Control_Scheme m_Wrapper;
        public Player_PCActions(@Player_Control_Scheme wrapper) { m_Wrapper = wrapper; }
        public InputAction @HorizontalMovement => m_Wrapper.m_Player_PC_HorizontalMovement;
        public InputAction @Attack => m_Wrapper.m_Player_PC_Attack;
        public InputAction @Jump => m_Wrapper.m_Player_PC_Jump;
        public InputAction @Dash => m_Wrapper.m_Player_PC_Dash;
        public InputActionMap Get() { return m_Wrapper.m_Player_PC; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player_PCActions set) { return set.Get(); }
        public void SetCallbacks(IPlayer_PCActions instance)
        {
            if (m_Wrapper.m_Player_PCActionsCallbackInterface != null)
            {
                @HorizontalMovement.started -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnHorizontalMovement;
                @HorizontalMovement.performed -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnHorizontalMovement;
                @HorizontalMovement.canceled -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnHorizontalMovement;
                @Attack.started -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnAttack;
                @Jump.started -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnJump;
                @Dash.started -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_Player_PCActionsCallbackInterface.OnDash;
            }
            m_Wrapper.m_Player_PCActionsCallbackInterface = instance;
            if (instance != null)
            {
                @HorizontalMovement.started += instance.OnHorizontalMovement;
                @HorizontalMovement.performed += instance.OnHorizontalMovement;
                @HorizontalMovement.canceled += instance.OnHorizontalMovement;
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
            }
        }
    }
    public Player_PCActions @Player_PC => new Player_PCActions(this);
    public interface IPlayer_PCActions
    {
        void OnHorizontalMovement(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
    }
}
