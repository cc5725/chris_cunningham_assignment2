using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_door_scrip : MonoBehaviour
{

    public scp_Scene_Manager sceneManage;

    private void Start()
    {
        sceneManage = FindObjectOfType<scp_Scene_Manager>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            sceneManage.LoadNextScene();
        }
        
    }
}
