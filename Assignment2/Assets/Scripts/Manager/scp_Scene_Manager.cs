using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scp_Scene_Manager : MonoBehaviour
{

    public static scp_Scene_Manager sceneManager;

    private void Awake()
    {
        if(sceneManager != null)
        {
            Destroy(this.gameObject);
        }
        else if (sceneManager == null)
        {
            sceneManager = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void closeGame()
    {
        Application.Quit();
    }
    public void LoadNextScene()
    {
        //Loads next index scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void loadWinScreen()
    {
        SceneManager.LoadScene("GameW");
    }
    
    public void loadLoss()
    {
        SceneManager.LoadScene("GameOver");
    }
    public void loadStart()
    {
        SceneManager.LoadScene("Level1");
    }
}
