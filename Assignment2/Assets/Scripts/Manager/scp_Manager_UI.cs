using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_Manager_UI : MonoBehaviour
{

    [Header("Player Variables")]
    //Represents player hp
    public Slider playerHealthSlider;

    //Reference to player life script
    scp_Player_LifeManager playerLifeManager;

    [Header("Score Variables")]
    public Text enemiesKilled;

    //reference game manager
    private scp_Manager_game gm;

    // Start is called before the first frame update
    void Start()
    {

        //Find and store script
        playerLifeManager = FindObjectOfType<scp_Player_LifeManager>();

        playerHealthSlider.maxValue = playerLifeManager.Life;
        playerHealthSlider.value = playerLifeManager.Life;

        gm = FindObjectOfType<scp_Manager_game>();
        enemiesKilled.text = gm.enemiesKilled.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        enemiesKilled.text = gm.enemiesKilled.ToString();
        playerHealthSlider.value = playerLifeManager.Life;
    }
}
