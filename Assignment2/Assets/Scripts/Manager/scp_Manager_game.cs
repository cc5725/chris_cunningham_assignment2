using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_game : MonoBehaviour
{

    public int enemiesKilled = 0;

    public static scp_Manager_game _instance;
    public scp_Scene_Manager sceneManager;

    public int winCondition = 2;


    private void Awake()
    {
        sceneManager = FindObjectOfType<scp_Scene_Manager>();
        if (_instance != null)
        {
            Destroy(this.gameObject);
        }
        else if (_instance == null)
        {
            _instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }
    public void EnemyKilledPlusOne()
    {
        //Increase enemies killed var
        enemiesKilled++;
    }

    private void Update()
    {
        if (enemiesKilled >= winCondition)
        {
            sceneManager.loadWinScreen();
            winCondition = 77777777;
        }
    }
}
