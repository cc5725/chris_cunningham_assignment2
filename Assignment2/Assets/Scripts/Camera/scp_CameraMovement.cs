using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_CameraMovement : MonoBehaviour
{
    //Object to follow
    public Transform target;

    //Camera offset variable
    public Vector3 cameraOffset = new Vector3(0,0,-10);

    //Smoothing variable for LERP
    public float smoothSpeed = 0.125f;

    private void FixedUpdate()
    {
        CameraFollow();
    }
    void CameraFollow()
    {
        //Store desired position
        Vector3 desiredPosition = target.position + cameraOffset;

        //Smooth position
        Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);


        transform.position = smoothPosition;
    }
}
