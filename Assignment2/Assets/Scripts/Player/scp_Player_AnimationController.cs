using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_AnimationController : MonoBehaviour
{

    //Animator reference
    private Animator anim;

    //Ref to player movement script
    private scp_Player_movement movement;

    // Start is called before the first frame update
    void Start()
    {
        //Find Animator for player and put it into anim 
        anim = GetComponent<Animator>();
        //Finds the script for movement and puts it into movement
        movement = GetComponent<scp_Player_movement>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementParameter();
        //Debug.Log(movement.horizontalValue);
    }

    void MovementParameter()
    {
        //Find asolute value of number 
        int absoluteValue = Mathf.Abs((int)movement.horizontalValue);
        //sets param to be horizontal movement value
        anim.SetInteger("Movement", absoluteValue);
    }

    public void Attack1()
    {
        anim.SetTrigger("Attack1");
    }

    public void Death()
    {
        anim.SetTrigger("Death");
    }

    public void Hurt()
    {
        anim.SetTrigger("Damaged");
    }

    public void Jump()
    {
        anim.SetTrigger("Jump");
    }
}
