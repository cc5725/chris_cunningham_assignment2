using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Attack : MonoBehaviour
{

    //Sword blade
    public Transform overlapCirclePoint;

    //Radius of the circle
    public float circleRadius;

    //Layer to hit
    public LayerMask enemyMask;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttackLogic()
    {
        // returns enemies in the range of the circle
       Collider2D[] enemyColliders = Physics2D.OverlapCircleAll(overlapCirclePoint.position, circleRadius, enemyMask);
    
        //Go through each enemy collision and print their name
        foreach (Collider2D enemy in enemyColliders)
        {
            Debug.Log(enemy.name);

            if (enemy.GetComponent<scp_Goblin_Master>())
            {
               var goblin = enemy.GetComponent<scp_Goblin_Master>();

                goblin.GetDamaged(1);
            }
        }
    
    }


    private void OnDrawGizmosSelected()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
    }

}
