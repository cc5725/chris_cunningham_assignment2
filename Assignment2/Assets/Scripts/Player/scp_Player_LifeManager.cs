using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_LifeManager : MonoBehaviour
{
    //Player life variable
    public int Life = 20;
    //Anim manager ref
    public scp_Player_AnimationController animController;

    //Varaible to store player status
    public bool isPlayerAlive = true;



    private void Start()
    {
        animController = GetComponent<scp_Player_AnimationController>();
    }


    public void RemoveLife(int hpToRemove)
    {
        Life -= hpToRemove;

       

        if (Life <= 0)
        {
            Death();
            
        }
        else
        {
            animController.Hurt();
        }
    }

    

    public void Death()
    {

        animController.Death();
        //Deactivate collider and rb and controls
        GetComponent<scp_Player_movement>().enabled = false;
        Destroy(GetComponent<Rigidbody2D>());
        GetComponent<Collider2D>().enabled = false;
        //Makes sure player dead
        isPlayerAlive = false;

        StartCoroutine(DeathAnimationScreen());
    }

    IEnumerator DeathAnimationScreen()
    {
        yield return new WaitForSeconds(3f);
        FindObjectOfType<scp_Scene_Manager>().loadLoss();
    }
}
