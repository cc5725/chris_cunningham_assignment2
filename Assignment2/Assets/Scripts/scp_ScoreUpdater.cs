using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_ScoreUpdater : MonoBehaviour
{
    public scp_Manager_game gameMan;
    public Text score;
    // Start is called before the first frame update
    void Start()
    {
        gameMan = FindObjectOfType<scp_Manager_game>();

       


        score.text = gameMan.enemiesKilled.ToString();
    }

}
