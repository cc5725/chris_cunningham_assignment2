using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Goblin_AnimationManager : scp_Enemy_Animation_Master
{
    public Color Damaged;
    public Color NearDeath;

    //Reference to sprite renderer
    SpriteRenderer goblinRenderer;

    protected override void Start()
    {
        base.Start();
        goblinRenderer = GetComponent<SpriteRenderer>();
    }
    public void AttackAnimation()
    {
        anim.SetTrigger("Attack");
    }
    
    public void DeathAnimation()
    {
        anim.SetTrigger("Death");
    }

    public void DamagedAnimation()
    {
        anim.SetTrigger("Damaged");
    }

    public void SetMovementParamter(int movement)
    {
        anim.SetInteger("Movement", movement);
    }

    public void Damage(int damageLevel)
    {
       // Debug.Log("Damage firing");
        switch (damageLevel)
        {
            case 2:
                goblinRenderer.color = Damaged;
                break;
            case 1:
                goblinRenderer.color = NearDeath;
                break;
        }
    }
}
