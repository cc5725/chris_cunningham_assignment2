using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Goblin_AwarenessF : MonoBehaviour
{

    //Reference to movement script

    public scp_Enemy_GoblinMovement goblinMove;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //attack 
           // StartCoroutine(SpeedUpForSetTime(3));
        }
    }

    IEnumerator SpeedUpForSetTime(float time)
    {
        goblinMove.speed = 25;
        yield return new WaitForSeconds(time);
        goblinMove.speed = goblinMove.originalSpeed;
    }
}
