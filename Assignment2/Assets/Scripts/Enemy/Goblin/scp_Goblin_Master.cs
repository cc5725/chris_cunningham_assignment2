using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Goblin_Master : scp_EnemyMaster
{

    //Reference animator
    scp_Enemy_Goblin_AnimationManager GoblinAnim;

    scp_Enemy_GoblinMovement goblinMove;

    protected override void Start()
    {
        base.Start();
        //Gets the gobin animation manager and goblin movement scripts and stores in a var
        GoblinAnim = GetComponent<scp_Enemy_Goblin_AnimationManager>();

        goblinMove = GetComponent<scp_Enemy_GoblinMovement>();
    }


    public override void Death()
    {
 
      GoblinAnim.DeathAnimation();
        gm.EnemyKilledPlusOne();

    }


    public override void GetDamaged(int healthLost)
    {
        base.GetDamaged(healthLost);

        //Fire get damaged animation
        if (enemyHealth > 0)
        {

            GoblinAnim.DamagedAnimation();
            GoblinAnim.Damage(enemyHealth);
        }
        else
        {
            Death();
            goblinMove.DeathStop();
        }
    }

  
}
