using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Goblin_AwarenessB : MonoBehaviour
{
    //Movement script ref
    public scp_Enemy_GoblinMovement goblinMove;

  

    private void OnTriggerEnter2D(Collider2D collision)
    {
       // Debug.Log("Entered collision");
        if (collision.gameObject.tag == "Player")
        {
           // Debug.Log("Tapped");
            //Flip me 
            goblinMove.FlipInstantly();
        }
    }
}
