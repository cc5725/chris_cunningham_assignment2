using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Goblin_Attack : MonoBehaviour
{
    [Header("Attack variables")]

    public int dmgVar = 5;

    [Header("References")]
    //Anim reference
    public scp_Enemy_Goblin_AnimationManager goblinAnim;

    [Header("Overlap Circle Variables")]

    //Sword blade
    public Transform overlapCirclePoint;

    //Radius of the circle
    public float circleRadius;

    //Layer to hit
    public LayerMask enemyMask;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            
            goblinAnim.AttackAnimation();
            //attack the player
            AttackLogic();

            Debug.Log("Player in range");
        }
    }

    public void AttackLogic()
    {
        // returns enemies in the range of the circle
        Collider2D playerCollider = Physics2D.OverlapCircle(overlapCirclePoint.position, circleRadius, enemyMask);
        Debug.Log(playerCollider);

        if (playerCollider.GetComponent<scp_Player_LifeManager>())
        {
            Debug.Log("Player damaged");
            playerCollider.GetComponent<scp_Player_LifeManager>().RemoveLife(dmgVar);
        }

    }

    private void OnDrawGizmosSelected()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
    }
}
