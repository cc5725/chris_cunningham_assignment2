using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_GoblinMovement : MonoBehaviour
{

    [SerializeField] Rigidbody2D goblinRB;

    public int direction = 1;

    public float speed;

    scp_Enemy_Goblin_AnimationManager goblinAnim;

    public float originalSpeed;

    private bool canMove = true;


    // Start is called before the first frame update
    void Start()
    {
        goblinRB = GetComponent<Rigidbody2D>();
        goblinAnim = GetComponent<scp_Enemy_Goblin_AnimationManager>();
        originalSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            MoveGoblin();
        }
    }

    void MoveGoblin()
    {

        Vector2 force = new Vector2(direction * speed * Time.deltaTime, 0);


        //Add force to rigidbody
        goblinRB.AddForce(force, ForceMode2D.Impulse);

        goblinAnim.SetMovementParamter((int)speed);
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Waypoint")
        {
            speed = 0;

            Invoke("FlipGoblin", 2f);

        }
    }

    private void FlipGoblin()
    {

        direction *= -1;
        speed = originalSpeed;

        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);

        // if (GetComponent<SpriteRenderer>().flipX == true)
        //  {
        //     GetComponent<SpriteRenderer>().flipX = false;
        //  }
        // else
        //  {
        //     GetComponent<SpriteRenderer>().flipX = true;
        //  }
    }

    public void FlipInstantly()
    {
        direction *= -1;
        speed = 0;
        canMove = false;
        goblinAnim.SetMovementParamter((int)speed);


        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
    }

    public IEnumerator StopEnemyForTime(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
    public void DeathStop()
    {
        canMove = false;
    }
}
