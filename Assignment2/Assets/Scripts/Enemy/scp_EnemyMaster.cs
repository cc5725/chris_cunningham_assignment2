using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyMaster : MonoBehaviour
{
    public int enemyHealth;

    public Sprite enemySprite;

    public string enemyName;

    protected scp_Manager_game gm;

    protected virtual void Start()
    {
        gm = FindObjectOfType<scp_Manager_game>();
    }


    public virtual void Death()
    {
        if (enemyHealth <= 0)
        {
            //Just die lol 
        }
    }

    public virtual void GetDamaged(int healthLost)
    {
        //Takes health from da enemy 
        enemyHealth -= healthLost;
    }

   protected void SelfDestruct(float timeBeforeDeath)
    {
        Destroy(this.gameObject, timeBeforeDeath);
    }
}
